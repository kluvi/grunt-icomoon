/*
 * grunt-icomoon
 * https://github.com/kluvi/grunt-icomoon
 *
 * Copyright (c) 2015 Jakub Kluvánek
 * Licensed under the MIT license.
 */

// 'use strict';

module.exports = function(grunt) {
	grunt.registerMultiTask('icomoon', 'Grunt task to generate LESS mixins from Icomoon JSON files.', function() {
		
		default_mixinPrefix = 'ico-';
		default_mixinExtender = 
				".<mixinPrefix>mixin-extender\n"+
				"{\n"+
				"\tfont-family: 'icomoon';\n"+
				"\tspeak: none;\n"+
				"\tfont-style: normal;\n"+
				"\tfont-weight: normal;\n"+
				"\tfont-variant: normal;\n"+
				"\ttext-transform: none;\n"+
				"\ttext-shadow: none !important;\n"+
				"\t-webkit-font-smoothing: antialiased;\n"+
				"\t-moz-osx-font-smoothing: grayscale;\n"+
				"}\n\n";
		default_mixinContent = 
				".<mixinPrefix><iconName>(@size: 0, @place: \":before\", @rules: 0, @includeExtender: 0) {\n"+
				"\t@place2: ~\"@{place}\";\n"+
				"\t&@{place2} {\n"+
				"\t\t&:extend(.<mixinPrefix>mixin-extender all);\n"+
				"\t\t& when not(@size = 0) {\n"+
				"\t\t\tfont-size: @size;\n"+
				"\t\t}\n"+
				"\t\t& when not(@rules = 0) {\n"+
				"\t\t\t@rules();\n"+
				"\t\t}\n"+
				"\t\t& when (@includeExtender = 1) {\n"+
				"\t\t\t.<mixinPrefix>mixin-extender;\n"+
				"\t\t}\n"+
				"\t\tcontent: \"\\<iconCode>\";\n"+
				"\t}\n"+
				"}\n"+
				"\n";

		var jsonPath = this.data.json;
		var outputPath = this.data.output;
		var json = grunt.file.readJSON(jsonPath);
		mixinPrefix = '';
		mixinExtender = '';
		mixinContent = '';

		if(typeof this.data.helperPrefix != "undefined")
		{
			mixinPrefix = this.data.helperPrefix;
		}

		if(typeof this.data.mixinPrefix != "undefined")
		{
			mixinPrefix = this.data.mixinPrefix;
		}

		if(typeof this.data.mixinExtender != "undefined")
		{
			mixinExtender = this.data.mixinExtender;
		}

		if(typeof this.data.mixinContent != "undefined")
		{
			mixinContent = this.data.mixinContent;
		}

		if(mixinPrefix.length == 0)
			mixinPrefix = '<default_mixinPrefix>';

		if(mixinExtender.length == 0)
			mixinExtender = '<default_mixinExtender>';

		if(mixinContent.length == 0)
			mixinContent = '<default_mixinContent>';

		mixinPrefix = mixinPrefix.replace(new RegExp('<default_mixinPrefix>', 'g'),default_mixinPrefix);

		mixinExtender = mixinExtender.replace(new RegExp('<default_mixinExtender>', 'g'),default_mixinExtender);
		mixinExtender = mixinExtender.replace(new RegExp('<mixinPrefix>', 'g'),mixinPrefix);

		mixinContent = mixinContent.replace(new RegExp('<default_mixinContent>', 'g'),default_mixinContent);
		mixinContent = mixinContent.replace(new RegExp('<mixinPrefix>', 'g'),mixinPrefix);

		var outputContent = '';
		outputContent += mixinExtender;

		json.icons.forEach(function(f){
			var code = f.properties.code.toString(16);
			var names = f.properties.name.split(',');
			names.forEach(function(name){
				name = name.trim();

				content = mixinContent;
				content = content.replace(new RegExp('<iconName>', 'g'),name);
				content = content.replace(new RegExp('<iconCode>', 'g'),code);
				outputContent += content;
			});
		});

		outputContent = outputContent.replace(new RegExp('<mixinPrefix>', 'g'),mixinPrefix);
		outputContent = outputContent.replace(new RegExp('<mixinExtender>', 'g'),mixinExtender);

		grunt.file.write(outputPath, outputContent);
	});
};