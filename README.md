# grunt-icomoon

[Grunt](http://gruntjs.com/) task to generate LESS mixins from [Icomoon](http://icomoon.io/) JSON files.

## Configuration
Just specify input JSON file and output mixins file. Other options are optional.
```js
grunt.initConfig({
    icomoon: {
      development: {
        json: 'less/fonts/selection.json',
        output: 'less/base/icomoon-mixins.less',
      },
    }
});
```
### Full configuration
```js
grunt.initConfig({
    icomoon: {
      development: {
        json: 'less/fonts/selection.json',
        output: 'less/base/icomoon-mixins.less',
        mixinPrefix: '<default_mixinPrefix>',
        mixinExtender: '<default_mixinExtender>',
        mixinContent: '<default_mixinContent>'
      },
    }
});
```
You can use following variables:
* option mixinPrefix: <default_mixinPrefix>
* option mixinExtender: <default_mixinExtender>, <mixinPrefix>
* option mixinContent: <default_mixinContent>, <mixinPrefix>, <iconName>, <iconCode>

### Default variable values
#### default_mixinPrefix
```js
"ico-""
```
#### default_mixinExtender
```js
".<mixinPrefix>mixin-extender\n"+
"{\n"+
"\tfont-family: 'icomoon';\n"+
"\tspeak: none;\n"+
"\tfont-style: normal;\n"+
"\tfont-weight: normal;\n"+
"\tfont-variant: normal;\n"+
"\ttext-transform: none;\n"+
"\ttext-shadow: none !important;\n"+
"\t-webkit-font-smoothing: antialiased;\n"+
"\t-moz-osx-font-smoothing: grayscale;\n"+
"}\n"
```
#### default_mixinContent
```js
".<mixinPrefix><iconName>(@size: 0, @place: \":before\", @rules: 0, @includeExtender: 0) {\n"+
"\t@place2: ~\"@{place}\";\n"+
"\t&@{place2} {\n"+
"\t\t&:extend(.<mixinPrefix>mixin-extender all);\n"+
"\t\t& when not(@size = 0) {\n"+
"\t\t\tfont-size: @size;\n"+
"\t\t}\n"+
"\t\t& when not(@rules = 0) {\n"+
"\t\t\t@rules();\n"+
"\t\t}\n"+
"\t\t& when (@includeExtender = 1) {\n"+
"\t\t\t.<mixinPrefix>mixin-extender;\n"+
"\t\t}\n"+
"\t\tcontent: \"\\<iconCode>\";\n"+
"\t}\n"+
"}\n"+
"\n"
```

### Configuring templates
If you want to add someting to every mixin, just use this:
```js
mixinContent: '<default_mixinContent> put your things here'
```


## Result
Task generates then file with mixins:
```less
.ico-name-of-icon(@size: 0, @place: ":before", @rules: 0) {
    @place2: ~"@{place}";
    &@{place2} {
        &:extend(.icon-mixin-extender all);
        & when not(@size = 0) {
            font-size: @size;
        }
        & when not(@rules = 0) {
            @rules();
        }
        & when (@includeExtender = 1) {
            .icon-mixin-extender;
        }
        content: "\e600";
    }
}
```

## Using fonts
1. generate font files on Icomoon
2. add @font-face to some LESS file
```css
@font-face {
    font-family: 'icomoon';
    src:url('xxx/icomoon.eot');
    src:url('xxx/icomoon.eot?#iefix') format('embedded-opentype'),
        url('xxx/icomoon.woff') format('woff'),
        url('xxx/icomoon.ttf') format('truetype'),
        url('xxx/icomoon.svg#icomoon') format('svg');
    font-weight: normal;
    font-style: normal;
}
```
3. generate mixins with this task and include them to your project
```less
@import "icomoon-mixins";
```
4. use them (see Examples)

## Examples
```less
#someElement
{
    .ico-arrow-up;
}

#someOtherElement
{
    .ico-arrow-down;
}

#differentSize
{
    .ico-arrow-up(10px);
}

#useAfter
{
    .ico-arrow-up(0, ":after");
}

#additionalRules
{
    .ico-arrow-up(0, ":after", {
        color: black;
    });
}

@media all
{
    #insideMedia // if we use mixin inside @media, the class .icon-mixin-extender is not available for extending (different scope) => set 4th parameter to 1
    {
        .ico-arrow-up(0, ":after",{},1);
    }
}
```
will generate
```css
#someElement:before
{
    content: "\e600";
}

#someOtherElement:before
{
    content: "\e601";
}

#differentSize:before
{
    font-size: 10px;
    content: "\e600";
}

#useAfter:after
{
    content: "\e600";
}

#additionalRules:after
{
    content: "\e600";
    color: black;
}

#insideMedia:after
{
    content: "\e600";
    font-family: 'icomoon';
    speak: none;
    font-style: normal;
    font-weight: normal;
    font-variant: normal;
    text-transform: none;
    text-shadow: none !important;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
}

#someElement:before,
#someOtherElement:before,
#differentSize:before,
#useAfter:after
{
    font-family: 'icomoon';
    speak: none;
    font-style: normal;
    font-weight: normal;
    font-variant: normal;
    text-transform: none;
    text-shadow: none !important;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
}
```

## Notes
* You can specify more icon names and separate them with coma (name1, name2, name3). Task generates 3 separate mixins.

## Changelog
* 2015-04-30 - 0.5.1
    * extend -> extend all
* 2015-04-29 - 0.5.0
    * added parameter for including mixin-extender
    * added mixinPrefix to mixin-extender
    * added option mixinExtender
    * added option mixinContent
* 2015-04-29 - 0.4.0
    * added support for additional ruleset
    * renamed option helperPrefix to mixinPrefix (helperPrefix still works)
* 2015-04-29 - 0.3.0
    * added support for multiple icon names
* 2015-04-28 - 0.2.0
    * added default value for font-size
    * added option helperPrefix
* 2015-04-28 - 0.1.0 - first release